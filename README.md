### <p align="center"><img width="150px" height="150px" style="border-radius:50%;box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.5)" src="https://cantinaband.gitlab.io/tabi-pwa/static-assets/logo512.png"></p>

# Tabi

Tabi is a Progressive Web App to find and share places for campers around the world.

## Available Scripts

In the project directory, you can run:

### `npm install`

Install all necessary node dependecies to run and build the project.

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />

### `npm build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

## Live Demo

The PWA is hostet on gitlab pages under: [cantinaband.gitlab.io/tabi-pwa/](https://cantinaband.gitlab.io/tabi-pwa/)
