import useLocalStorage from "../hooks/useLocalStorage";
import React, { Dispatch, createContext, ReactNode, useContext } from "react";

export type ColorMode = "light" | "dark";

function preferredColorMode(): ColorMode {
  const query = "(prefers-color-scheme: dark)";
  if (window.matchMedia(query).matches) {
    return "dark";
  }
  return "light";
}

export interface IColorModeContext {
  colorMode: ColorMode;
  setColorMode: Dispatch<React.SetStateAction<ColorMode>>;
}

export const ColorModeContext = createContext<IColorModeContext>({
  colorMode: "light",
  setColorMode: () => {
    throw new Error("not implemented");
  }
});

type Props = {
  children: ReactNode;
};
export default function ThemeProvider({ children }: Props) {
  const [colorMode, setColorMode] = useLocalStorage<ColorMode>(
    "colorMode",
    preferredColorMode()
  );
  return (
    <ColorModeContext.Provider value={{ colorMode, setColorMode }}>
      {children}
    </ColorModeContext.Provider>
  );
}

export function useColorMode() {
  return useContext(ColorModeContext);
}
