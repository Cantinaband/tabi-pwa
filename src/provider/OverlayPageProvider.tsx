import React, {
  ReactNode,
  useState,
  useContext,
  Dispatch,
  SetStateAction
} from "react";
import styled, { createGlobalStyle, css } from "styled-components";
import RoundClose from "../common/RoundClose";

export interface IOverlayPageContext {
  setOverlay: (e: ReactNode, title?: string) => void;
  setTitle: Dispatch<SetStateAction<string | undefined>>;
  closeOverlay: () => void;
}

export const OverlayPageContext = React.createContext<IOverlayPageContext>({
  setOverlay: () => {
    throw new Error("not implemented.");
  },
  closeOverlay: () => {
    throw new Error("not implemented.");
  },
  setTitle: () => {
    throw new Error("not implemented");
  }
});

export const OVERLAY_DURATION = 500;

const GlobalStyle = createGlobalStyle<{ open: boolean }>`
#root > div:first-of-type {
    transition: all ${OVERLAY_DURATION}ms ease-in-out;
}
${(p) =>
  p.open &&
  css`
    #root > div:first-of-type {
      border-radius: 8px 8px 0 0;
      transform: scale(0.95, 0.975);
    }
  `}  
`;

const BackDrop = styled.div<{ open: boolean }>`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.3);
  opacity: ${(p) => (p.open ? 1 : 0)};
  transition: opacity ${OVERLAY_DURATION}ms ease-in-out;
  pointer-events: none;
`;

const Container = styled.div<{ open: boolean }>`
  position: fixed;
  top: 2.5%;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  flex-direction: column;
  overflow: hidden;
  background-color: var(--navBackground);
  z-index: 3;
  transform: translateY(${(p) => (p.open ? 0 : 100)}%);
  transition: transform ${OVERLAY_DURATION}ms ease-in-out;
  will-change: transform;
  border-radius: 8px 8px 0 0;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: var(--headerBackground);
  padding: 16px;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  z-index: 1;
`;
const HeaderCol = styled.div`
  display: flex;
  flex: 1;
`;

const Close = styled(RoundClose)`
  box-shadow: none;
  background: var(--interactionBackground);
  margin-left: auto;
`;
const Title = styled.div`
  margin-left: auto;
  transform: translateX(50%);
`;

type Props = {
  children: ReactNode;
};

export default function OverlayPageProvider({ children }: Props) {
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState<ReactNode>();
  const [title, setTitle] = useState<string>();

  const setOverlay = (c: ReactNode, title?: string) => {
    setOpen(true);
    setContent(c);
    setTitle(title);
  };

  const closeOverlay = () => {
    setOpen(false);
    setTimeout(() => {
      setContent(undefined);
      setTitle(undefined);
    }, OVERLAY_DURATION);
  };
  return (
    <OverlayPageContext.Provider value={{ closeOverlay, setOverlay, setTitle }}>
      <GlobalStyle open={open} />
      {children}
      <BackDrop open={open} />
      <Container open={open}>
        <Header>
          <HeaderCol>
            <Title>{title}</Title>
          </HeaderCol>
          <HeaderCol>
            <Close onClick={closeOverlay} />
          </HeaderCol>
        </Header>
        {content}
      </Container>
    </OverlayPageContext.Provider>
  );
}

export function useOverlay() {
  return useContext(OverlayPageContext);
}
