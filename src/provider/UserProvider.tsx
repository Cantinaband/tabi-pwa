import React, {
  ReactNode,
  useState,
  useEffect,
  useContext,
  useRef
} from "react";
import { auth } from "../firebase";
import { useColorMode, ColorMode } from "./ThemeProvider";
import { createGlobalStyle } from "styled-components";
import { AppColors, colorModes } from "../colors";

const ColorStyles = createGlobalStyle<{
  mode: ColorMode;
}>`
  html {
    ${(p) => {
      const modeColors: AppColors = colorModes[p.mode];
      const colors = Object.keys(modeColors) as [keyof AppColors];
      return colors
        .map((e) => {
          const rgb = modeColors[e].map((e) => e).join(",");
          return `
            --${e}: rgb(${rgb});
            --${e}Rgb: ${rgb};
          `;
        })
        .join("");
    }}
  }
`;

interface UserContext {
  user: firebase.User | null;
  login: (
    email: string,
    password: string
  ) => Promise<firebase.auth.UserCredential | void>;
  signUp: (
    email: string,
    password: string
  ) => Promise<firebase.auth.UserCredential>;
  signOut: () => Promise<void>;
  deleteUser: () => Promise<void>;
}
export const UserContext = React.createContext<UserContext>({
  user: null,
  login: () => Promise.resolve({} as firebase.auth.UserCredential),
  signUp: () => Promise.resolve({} as firebase.auth.UserCredential),
  signOut: () => Promise.resolve(),
  deleteUser: () => {
    throw new Error("not implemented");
  }
});

type Props = {
  children: ReactNode;
};

export default function UserProvider({ children }: Props) {
  const [user, setUser] = useState<firebase.User | null>(null);
  const { colorMode } = useColorMode();
  const deleteAfterLogin = useRef(false);

  useEffect(() => {
    const unsubscribe = auth.onAuthStateChanged((userAuth) => {
      setUser(userAuth);
    });
    return () => unsubscribe();
  }, []);

  const login = (email: string, password: string) => {
    return auth.signInWithEmailAndPassword(email, password).then((e) => {
      if (deleteAfterLogin.current) {
        return deleteUser().then(() => e);
      }
      return e;
    });
  };

  const signUp = (email: string, password: string) => {
    return auth.createUserWithEmailAndPassword(email, password).then(() => {
      return login(email, password);
    });
  };

  const signOut = () => {
    return auth.signOut();
  };

  const deleteUser = () => {
    return user!
      .delete()
      .then()
      .catch((e) => {
        if (e.code === "auth/requires-recent-login") {
          signOut();
        }
      });
  };

  return (
    <UserContext.Provider value={{ user, login, signUp, signOut, deleteUser }}>
      <ColorStyles mode={colorMode} />
      {children}
    </UserContext.Provider>
  );
}

export function useUser() {
  return useContext(UserContext);
}
