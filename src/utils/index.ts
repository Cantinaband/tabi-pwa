import { Category } from "../typings/marker";

const lerp = (x: number, y: number, a: number) => x * (1 - a) + y * a;
const invlerp = (x: number, y: number, a: number) => clamp((a - x) / (y - x));
const clamp = (a: number, min = 0, max = 1) => Math.min(max, Math.max(min, a));

export function interpolateRange(
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  a: number
) {
  return lerp(x2, y2, invlerp(x1, y1, a));
}

export function getAllFilters() {
  return [
    {
      name: "beach",
      icon: "beach"
    },
    {
      name: "nature",
      icon: "woods"
    },
    { name: "parking spot", icon: "parking" },
    { name: "campsite", icon: "campsite" },
    { name: "forest", icon: "forest" },
    { name: "picnic area", icon: "picnic area" },
    { name: "camper parking", icon: "camper parking" }
  ] as Category[];
}

declare global {
  interface Navigator {
    standalone?: boolean;
  }
}

export const isIosDevice = () => {
  const userAgent = window.navigator.userAgent.toLowerCase();
  return /iphone|ipad|ipod/.test(userAgent);
};

export function isInstalled() {
  return (
    window.navigator.standalone === true ||
    window.matchMedia("(display-mode: standalone)").matches
  );
}
