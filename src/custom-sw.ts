/* eslint-disable no-restricted-globals */
// @ts-ignore
declare const workbox: any;

//Cache mapbox map-data requests
workbox.routing.registerRoute(
  new RegExp("https://api.mapbox.com/"),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "mapbox-map",
    plugins: [
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 30 // 30days
      })
    ]
  })
);

//Cache mapbox geolocoation search requests
workbox.routing.registerRoute(
  new RegExp("https://events.mapbox.com"),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: "mapbox-search",
    plugins: [
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 30 // 30days
      })
    ]
  })
);
