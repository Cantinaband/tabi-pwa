import { useEffect, useState } from "react";
import { getMarkers } from "../firebase";
import { Marker, Category } from "../typings/marker";

export default function useMarkers(filters: Category[]) {
  const [error, setError] = useState(false);
  const [markers, setMarkers] = useState<Marker[]>();

  useEffect(() => {
    const unsubscribe = getMarkers(filters).onSnapshot(
      (doc) => {
        const data = doc.docs.map((doc) => {
          return {
            ...doc.data(),
            id: doc.id
          } as Marker;
        });
        setMarkers(data);
      },
      (err) => {
        setError(Boolean(err.message));
      }
    );
    return () => unsubscribe();
  }, [filters]);

  return {
    error,
    markers
  };
}


