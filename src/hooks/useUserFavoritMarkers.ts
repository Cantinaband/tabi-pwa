import { useEffect, useState } from "react";
import { getFavoritesFromUser } from "../firebase";
import { Marker } from "../typings/marker";

export default function useUserFavoritMarkers(userId?: string) {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);
  const [markers, setMarkers] = useState<Marker[]>();

  useEffect(() => {
    if (userId) {
      const unsubscribe = getFavoritesFromUser(userId).onSnapshot(
        (doc) => {
          const data = doc.docs.map((doc) => {
            return {
              ...doc.data(),
              id: doc.id
            } as Marker;
          });
          setMarkers(data);
        },
        (err) => {
          setLoading(false);
          setError(Boolean(err.message));
        }
      );
      return () => unsubscribe();
    }
  }, [userId]);
  return {
    error,
    loading,
    markers
  };
}
