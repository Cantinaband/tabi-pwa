import { useEffect, useState } from "react";
import { set, get } from "idb-keyval";

export default function useImages(images?: string[]) {
  const [cachedImages, setCachedImages] = useState<string[]>([]);

  useEffect(() => {
    setCachedImages([]);
    images?.forEach((image) => {
      //use IndexedDB
      get(image).then((e) => {
        if (e) {
          setCachedImages((prev) => [...prev, URL.createObjectURL(e)]);
        } else {
          fetch(image)
            .then(function (response) {
              return response.blob();
            })
            .then(function (blob) {
              setCachedImages((prev) => [...prev, URL.createObjectURL(blob)]);
              set(image, blob);
            });
        }
      });
    });
  }, [images]);

  return {
    images: cachedImages
  };
}
