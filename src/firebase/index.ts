import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

import { AddMarker, Marker, Category } from "../typings/marker";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET
};

firebase.initializeApp(firebaseConfig);
firebase.firestore().enablePersistence();

export const auth = firebase.auth();
const db = firebase.firestore();

export const getMarkers = (filters: Category[]) => {
  if (!!filters.length) {
    return db.collection("Markers").where("category", "in", filters);
  }
  return db.collection("Markers");
};

export const addMarker = async (marker: AddMarker) => {
  const { images, description, category, geopoint, creator } = marker;

  const imageURLs = images.map(async (i) => {
    const storage = firebase.storage().ref().child(`images/${i.name}`);
    return (await storage
      .put(i)
      .then(() =>
        storage.getDownloadURL().then((url: string) => url)
      )) as string;
  });
  return Promise.all(imageURLs).then((e) =>
    db.collection("Markers").add({
      images: e,
      description,
      category,
      geopoint,
      creator
    })
  );
};

export const saveComment = async (
  rating: number,
  comment: string,
  marker: Marker,
  userId: string
) => {
  let newRatingValue = rating;

  if (marker.rating) {
    const overAll = marker.rating.value;
    const totalRatings = marker.rating.numberOfRatings;
    const newRating = rating;
    const newtotalRatings = marker.rating?.numberOfRatings + 1;
    newRatingValue = (overAll * totalRatings + newRating) / newtotalRatings;
  }
  const newRatingsNumber = marker.rating
    ? marker.rating?.numberOfRatings + 1
    : 1;

  return await db
    .collection("Markers")
    .doc(marker.id)
    .update({
      comments: firebase.firestore.FieldValue.arrayUnion({
        rating,
        message: comment,
        createdAt: firebase.firestore.Timestamp.now(),
        userId
      }),
      rating: {
        value: newRatingValue,
        numberOfRatings: newRatingsNumber
      }
    });
};

export const addToFavorites = (marker: Marker, userId: string) => {
  db.collection("Markers")
    .doc(marker.id)
    .update({
      favorites: firebase.firestore.FieldValue.arrayUnion(userId)
    });
};

export const removeFromFavorites = (marker: Marker, userId: string) => {
  db.collection("Markers")
    .doc(marker.id)
    .update({
      favorites: firebase.firestore.FieldValue.arrayRemove(userId)
    });
};

export const getAllFromUser = (userId: string) => {
  return db.collection("Markers").where("creator", "==", userId);
};

export const getFavoritesFromUser = (userId: string) => {
  return db.collection("Markers").where("favorites", "array-contains", userId);
};

export const deleteMarker = (marker: Marker) => {
  return db.collection("Markers").doc(marker.id).delete();
};
