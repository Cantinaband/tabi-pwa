type RGB = [number, number, number];

const golden: RGB = [240, 185, 5];
const white: RGB = [255, 255, 255];
const almostWhite: RGB = [246, 246, 246];
const darkWhite: RGB = [201, 201, 201];
const tintedBlack: RGB = [38, 38, 38];
const paleBlack: RGB = [70, 70, 70];
const shadedGray: RGB = [100, 100, 100];
const darkCoral: RGB = [199, 81, 70];

export interface AppColors {
  pageBackground: RGB;
  inputBackground: RGB;
  inputBorder: RGB;
  inputBorderFocus: RGB;
  buttonBackground: RGB;
  buttonText: RGB;
  text: RGB;
  rootBackground: RGB;
  headerBackground: RGB;
  filterBackground: RGB;
  filterBorder: RGB;
  interactionBackground: RGB;
  logoutBackground: RGB;
  warn: RGB;
  cardBackground: RGB;
  imageSliderBackground: RGB;
  mapButtonBackground: RGB;
  linkColor: RGB;
  icon: RGB;
  footerBackground: RGB;
  markerBackground: RGB;
}

export type ColorMode = "light" | "dark";

export const colorModes: Record<string, AppColors> = {
  light: {
    pageBackground: almostWhite,
    inputBackground: almostWhite,
    buttonBackground: golden,
    inputBorder: shadedGray,
    inputBorderFocus: tintedBlack,
    text: tintedBlack,
    buttonText: almostWhite,
    rootBackground: darkWhite,
    headerBackground: white,
    logoutBackground: white,
    filterBackground: almostWhite,
    filterBorder: darkWhite,
    interactionBackground: almostWhite,
    warn: darkCoral,
    cardBackground: white,
    imageSliderBackground: darkWhite,
    mapButtonBackground: white,
    linkColor: tintedBlack,
    icon: tintedBlack,
    footerBackground: darkWhite,
    markerBackground: tintedBlack
  },
  dark: {
    pageBackground: tintedBlack,
    inputBackground: tintedBlack,
    buttonBackground: golden,
    inputBorder: darkWhite,
    inputBorderFocus: almostWhite,
    text: almostWhite,
    buttonText: almostWhite,
    rootBackground: darkWhite,
    headerBackground: paleBlack,
    logoutBackground: shadedGray,
    filterBackground: shadedGray,
    filterBorder: darkWhite,
    interactionBackground: shadedGray,
    warn: darkCoral,
    cardBackground: paleBlack,
    imageSliderBackground: shadedGray,
    mapButtonBackground: paleBlack,
    linkColor: shadedGray,
    icon: almostWhite,
    footerBackground: paleBlack,
    markerBackground: tintedBlack
  }
};
