import MapSlider from "./MapSlider";
import React, { RefObject, useRef } from "react";
import MapContainer from "./MapContainer";

type Props = {
  menuRef: RefObject<HTMLDivElement>;
};
export default function Map({ menuRef }: Props) {
  const locateRef = useRef<HTMLDivElement>(null);

  return (
    <>
      <MapSlider locateRef={locateRef} menuRef={menuRef} />
      <MapContainer ref={locateRef} />
    </>
  );
}
