import { SearchResult } from "../common/GeoAutocomplete";
import React, {
  ReactNode,
  useState,
  useContext,
  Dispatch,
  SetStateAction,
  useEffect
} from "react";
import { Category, Marker } from "../typings/marker";
import { SliderState } from "./MapSlider";
import useMarkers from "../hooks/useMarkers";

interface MapContext {
  searchResult?: SearchResult;
  setSearchResult: Dispatch<SetStateAction<SearchResult | undefined>>;
  setSelectedFilters: Dispatch<SetStateAction<Category[]>>;
  mapSliderState: SliderState;
  setMapSliderState: Dispatch<SetStateAction<SliderState>>;
  selectedMarker?: Marker;
  setSelectedMarker: (marker?: Marker) => void;
  addPlaceLocationMode: boolean;
  setAddPlaceLocationMode: Dispatch<SetStateAction<boolean>>;
  addPlaceLocation?: { lat: number; lng: number };
  setAddPlaceLocation: Dispatch<
    SetStateAction<{ lat: number; lng: number } | undefined>
  >;
  markers?: Marker[];
}

export const MapContext = React.createContext<MapContext>({
  setSearchResult: () => null,
  searchResult: undefined,
  setSelectedFilters: () => {
    throw new Error("not implemented");
  },
  mapSliderState: SliderState.CLOSE,
  setMapSliderState: () => null,
  setSelectedMarker: () => null,
  addPlaceLocationMode: false,
  setAddPlaceLocationMode: () => {
    throw new Error("not implemented");
  },
  addPlaceLocation: undefined,
  setAddPlaceLocation: () => null
});

type Props = {
  children: ReactNode;
};

export default function MapProvider({ children }: Props) {
  const [searchResult, setSearchResult] = useState<SearchResult>();

  const [selectedFilters, setSelectedFilters] = useState<Category[]>([]);

  const [selectedMarker, setSelectedMarker] = useState<Marker>();

  const [mapSliderState, setMapSliderState] = useState<SliderState>(
    SliderState.CLOSE
  );

  const [addPlaceLocationMode, setAddPlaceLocationMode] = useState(false);

  const [addPlaceLocation, setAddPlaceLocation] = useState<{
    lat: number;
    lng: number;
  }>();

  const { markers } = useMarkers(selectedFilters);

  useEffect(() => {
    if (markers && selectedMarker) {
      const index = markers?.findIndex((e) => e.id === selectedMarker?.id);
      setSelectedMarker(markers[index]);
    }
  }, [markers, selectedMarker]);

  const setSelectedMarkerHandler = (marker?: Marker) => {
    if (marker && marker === selectedMarker) {
      setMapSliderState(SliderState.MEDIUM);
    }
    setSelectedMarker(marker);
  };

  return (
    <MapContext.Provider
      value={{
        setSearchResult,
        searchResult,
        setSelectedFilters,
        mapSliderState,
        setMapSliderState,
        selectedMarker,
        setSelectedMarker: setSelectedMarkerHandler,
        addPlaceLocationMode,
        setAddPlaceLocationMode,
        addPlaceLocation,
        setAddPlaceLocation,
        markers
      }}
    >
      {children}
    </MapContext.Provider>
  );
}

export function useMap() {
  return useContext(MapContext);
}
