import React, { useRef, useEffect } from "react";

import ReactMapboxGl, { MapContext, Layer, Feature } from "react-mapbox-gl";
import usePosition from "../hooks/usePosition";
import styled from "styled-components";
import { Compass } from "../common/Icon";
import { SLIDER_DURATION, SliderState } from "./MapSlider";
import Button from "../common/Button";
import { useMap } from "./MapProvider";
import AddPlace from "../pages/AddPlace";
import { useOverlay } from "../provider/OverlayPageProvider";
import pin1x from "./pin@1x.png";
import pin2x from "./pin@2x.png";
import pin3x from "./pin@3x.png";

export const BOTTOM_BUTTON_DISTANCE = "3.2rem";

const Mapbox = ReactMapboxGl({
  accessToken: process.env.REACT_APP_MAPBOX_ACCESS_TOKEN || "",
  attributionControl: false
});

const LocateButton = styled.div`
  padding: 12px;
  box-sizing: border-box;
  position: absolute;
  bottom: ${BOTTOM_BUTTON_DISTANCE};
  left: 16px;
  width: 48px;
  height: 48px;
  border-radius: 100%;
  background: var(--mapButtonBackground);
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  transition: transform ${SLIDER_DURATION}ms ease-out;
`;

const SelectButton = styled(Button)`
  position: absolute;
  bottom: 32px;
  left: 50%;
  transform: translateX(-50%);
`;

const Pin = styled.svg`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate3d(-50%, -100%, 0);
  width: 2rem;
`;

const MapContainer = React.forwardRef((_, ref: React.Ref<HTMLDivElement>) => {
  const mapCenter = useRef<[number, number]>([9.446996, 54.793743]);
  const mapZoom = useRef(5);
  const mapRef = useRef<any>(null);
  const { latitude, longitude, error } = usePosition();
  const {
    markers,
    setSelectedMarker,
    setMapSliderState,
    addPlaceLocationMode,
    setAddPlaceLocation
  } = useMap();
  const { setOverlay } = useOverlay();

  const markerIcon = new Image(31, 40);
  markerIcon.srcset = `${pin1x} 1x,${pin2x} 2x, ${pin3x} 3x`;

  const images: any = [["marker", markerIcon]];

  const { searchResult } = useMap();

  useEffect(() => {
    if (mapRef.current && searchResult) {
      mapRef.current.state.map.flyTo({
        center: searchResult.center,
        zoom: addPlaceLocationMode ? 17 : 11
      });
    }
  }, [addPlaceLocationMode, searchResult]);

  return (
    <Mapbox
      // eslint-disable-next-line react/style-prop-object
      style="mapbox://styles/mapbox/streets-v11"
      ref={mapRef}
      center={mapCenter.current}
      zoom={[mapZoom.current]}
      containerStyle={{ height: "100%" }}
      onMove={(map) => {
        const center = map.getCenter();
        mapCenter.current = [center.lng, center.lat];
        mapZoom.current = map.getZoom();
        setMapSliderState((prev) =>
          prev === SliderState.MEDIUM ? SliderState.SMALL : prev
        );
      }}
      onStyleLoad={(e) => {
        e.on("click", ({ originalEvent }: { originalEvent: Event }) => {
          if (!originalEvent.defaultPrevented) {
            setMapSliderState(SliderState.CLOSE);
            setTimeout(() => {
              setSelectedMarker(undefined);
            }, SLIDER_DURATION);
          }
        });
      }}
    >
      <>
        {addPlaceLocationMode && (
          <Pin
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 24"
          >
            <circle
              cx="7"
              cy="6"
              r="4.5"
              stroke="var(--markerBackground)"
              strokeWidth="3"
            />
            <path
              fill="var(--markerBackground)"
              d="M7 12S1.5 3.5 1 6s3.5 18 6 18 6.5-16 6-18-6 6-6 6z"
            />
          </Pin>
        )}
      </>
      <MapContext.Consumer>
        {(map) => (
          <>
            {!error && latitude && longitude && (
              <LocateButton
                ref={ref}
                onClick={() =>
                  map.flyTo({ center: [longitude, latitude], zoom: 13 })
                }
              >
                <Compass />
              </LocateButton>
            )}
            {addPlaceLocationMode && (
              <SelectButton
                onClick={() => {
                  const center = map.getCenter();
                  setAddPlaceLocation({ lat: center.lat, lng: center.lng });
                  setOverlay(<AddPlace />);
                }}
              >
                select
              </SelectButton>
            )}
          </>
        )}
      </MapContext.Consumer>
      {
        <Layer
          type="symbol"
          id="marker"
          layout={{
            "icon-image": "marker",
            "icon-anchor": "bottom",
            "icon-allow-overlap": true
          }}
          images={images}
        >
          {markers &&
            !addPlaceLocationMode &&
            markers.map((m, i) => (
              <Feature
                key={i}
                onClick={(
                  e: React.MouseEvent<HTMLElement, MouseEvent> & {
                    originalEvent: Event;
                  }
                ) => {
                  e.originalEvent.preventDefault();
                  setSelectedMarker(m);
                }}
                coordinates={[m.geopoint.longitude, m.geopoint.latitude]}
              />
            ))}
        </Layer>
      }
    </Mapbox>
  );
});

export default MapContainer;
