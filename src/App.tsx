import React from "react";

import { createGlobalStyle } from "styled-components";
import Home from "./pages/Home";
import UserProvider from "./provider/UserProvider";
import MapProvider from "./Map/MapProvider";
import OverlayPageProvider from "./provider/OverlayPageProvider";
import ThemeProvider from "./provider/ThemeProvider";

const GlobalStyle = createGlobalStyle`
  html {
    margin: 0;
  }
  body {
    position: fixed;
    top: 0;
    left: 0; 
    right: 0;
    bottom: 0;
    margin: 0;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, Arial, Helvetica, Verdana;
    overflow: hidden;
    color: var(--text);
  }
  #root {
    position: absolute;
    overflow-y: hidden;
    top: 0;
    left: 0; 
    right: 0;
    bottom: 0;
    background: var(--rootBackground);
  }
`;

function App() {
  return (
    <ThemeProvider>
      <UserProvider>
        <MapProvider>
          <OverlayPageProvider>
            <GlobalStyle />
            <Home />
          </OverlayPageProvider>
        </MapProvider>
      </UserProvider>
    </ThemeProvider>
  );
}

export default App;
