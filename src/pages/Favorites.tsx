import React from "react";
import styled from "styled-components";
import { useUser } from "../provider/UserProvider";
import useUserFavoritMarkers from "../hooks/useUserFavoritMarkers";
import MarkerCard from "../common/MarkerCard";
import AuthenticatedPage from "../common/AuthenticatedPage";
import Empty from "../common/Empty";

const MarkerList = styled.div`
  padding-bottom: 16px;
  & > * {
    margin: 0;
  }
  & > * + * {
    margin-top: 16px;
  }
`;

export default function Favorites() {
  const { user } = useUser();
  const { markers } = useUserFavoritMarkers(user?.uid);
  return (
    <AuthenticatedPage padded title="Favorites" withFooter>
      <h2>Your Favorites</h2>
      <MarkerList>
        {markers?.length ? (
          markers?.map((m, i) => (
            <MarkerCard favoriteButton marker={m} key={i} />
          ))
        ) : (
          <Empty>you have no favorite places :(</Empty>
        )}
      </MarkerList>
    </AuthenticatedPage>
  );
}
