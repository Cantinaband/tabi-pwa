import Page from "../common/Page";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import TextArea from "../common/TextArea";
import styled from "styled-components";
import FileField from "../common/FileField";
import HorizontalSlider from "../common/HorizontalSlider";
import RoundClose from "../common/RoundClose";
import Button from "../common/Button";
import { Category } from "../typings/marker";
import { getAllFilters } from "../utils";
import { addMarker } from "../firebase";
import FullScreenLoading from "../common/FullScreenLoading";
import { useMap } from "../Map/MapProvider";
import firebase from "firebase";
import { useUser } from "../provider/UserProvider";
import { useOverlay } from "../provider/OverlayPageProvider";
import Filters from "../common/Filters";
import AnimatedCheck from "../common/AnimatedCheck";

const Form = styled.form`
  margin-bottom: 16px;
`;

const AddImages = styled(FileField)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: 0 auto;
  border: 2px solid var(--inputBorder);
  box-sizing: border-box;
  width: 20vw;
  height: 20vw;
  border-radius: 100%;
  padding: 16px;
`;
const ImageSlider = styled(HorizontalSlider)`
  height: 30vh;
  margin: 32px 0;
`;

const ImageContainer = styled.div`
  position: relative;
  margin-right: 16px;
`;

const SliderImage = styled.img`
  width: 65vw;
  object-fit: cover;
  height: 100%;
`;

const StyledFilters = styled(Filters)`
  margin-bottom: 32px;
`;

const StyledTextArea = styled(TextArea)`
  margin: 20px 16px;
`;

const ImageDelete = styled(RoundClose)`
  position: absolute;
  top: 1rem;
  right: 1rem;
  background: var(--warn);
`;

const StyledButton = styled(Button)`
  display: block;
  margin: 32px auto;
`;

const Success = styled.div`
  display: flex;
  flex: 1 0 auto;
  align-items: center;
  justify-content: center;
  padding: 4em;
`;
type FormData = {
  description?: string;
};

export default function AddPlace() {
  const { register, handleSubmit, getValues } = useForm<FormData>();
  const [images, setImages] = useState<File[]>([]);
  const [category, setCategory] = useState<Category>();
  const [pending, setPending] = useState(false);
  const [success, setSuccess] = useState(false);

  const {
    addPlaceLocation,
    setAddPlaceLocation,
    setAddPlaceLocationMode
  } = useMap();

  const { user } = useUser();

  const { closeOverlay } = useOverlay();

  const onSubmit = handleSubmit(async ({ description }) => {
    setPending(true);
    await addMarker({
      category: category!,
      description: description!,
      images,
      geopoint: new firebase.firestore.GeoPoint(
        addPlaceLocation!.lat,
        addPlaceLocation!.lng
      ),
      creator: user!.uid
    })
      .then(() => {
        setPending(false);
        setAddPlaceLocation(undefined);
        setAddPlaceLocationMode(false);
        setSuccess(true);
        setTimeout(() => {
          closeOverlay();
        }, 2500);
      })
      .catch(() => setPending(false));
  });

  const removeImage = (index: number) => {
    let data = [...images];
    data.splice(index, 1);
    setImages(data);
  };

  const canSubmit = () => {
    return !!images.length && !!category && !!getValues("description");
  };

  return (
    <Page title="Add Place">
      {!success ? (
        <Form onSubmit={onSubmit}>
          <StyledFilters
            padding="16px"
            filter={getAllFilters()}
            onFilterChange={(e) => setCategory(e[0])}
          />
          <StyledTextArea
            placeholder="Description..."
            name="description"
            ref={register({ required: true })}
          />
          {!!images.length && (
            <ImageSlider padding="0 16px">
              {images.map((e, index) => {
                const url = URL.createObjectURL(e);
                return (
                  <ImageContainer key={index}>
                    <SliderImage src={url} />
                    <ImageDelete onClick={() => removeImage(index)} />
                  </ImageContainer>
                );
              })}
            </ImageSlider>
          )}
          <AddImages
            name="images"
            onChange={(files) => {
              setImages((prev) => [...prev, ...files]);
            }}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 46 35"
            >
              <path
                stroke="var(--icon)"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M34.5 34H45V8.096H34.5S34.5 5.5 31 3 22.5.5 19 3s-3.5 5.096-3.5 5.096h-4s.189.186-.088-.596c-.782-2.209-5.218-2.209-6 0-.276.782.088.596.088.596H1V34h14.5"
              />
              <circle
                cx="25.5"
                cy="22.5"
                r="11.5"
                stroke="var(--icon)"
                strokeWidth="2"
              />
              <path
                fill="var(--icon)"
                d="M16.035 22.802a1 1 0 001.96.396l-1.96-.396zm13.607-6.386a1 1 0 00.759-1.85l-.76 1.85zm-11.646 6.782c.569-2.811 1.602-4.56 2.752-5.648 1.149-1.087 2.49-1.585 3.804-1.76 1.324-.178 2.602-.023 3.564.184a11.669 11.669 0 011.439.409l.072.027.015.006h.001l.378-.925a88.224 88.224 0 00.378-.926l-.004-.002-.01-.003a4.435 4.435 0 00-.136-.053 13.69 13.69 0 00-1.711-.488c-1.101-.238-2.625-.43-4.252-.212-1.637.219-3.397.857-4.912 2.29-1.514 1.432-2.707 3.586-3.339 6.704l1.96.398z"
              />
            </svg>
          </AddImages>
          <StyledButton disabled={!canSubmit()} type="submit">
            save
          </StyledButton>
        </Form>
      ) : (
        <Success>
          <AnimatedCheck />
        </Success>
      )}
      {pending && <FullScreenLoading />}
    </Page>
  );
}
