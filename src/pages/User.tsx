import React from "react";
import { useUser } from "../provider/UserProvider";
import useUserMarkers from "../hooks/useUserMarkers";
import MarkerCard from "../common/MarkerCard";
import styled from "styled-components";
import AuthenticatedPage from "../common/AuthenticatedPage";
import { useOverlay } from "../provider/OverlayPageProvider";
import Empty from "../common/Empty";

const MarkerList = styled.div`
  padding-bottom: 16px;
  & > * {
    margin: 0;
  }
  & > * + * {
    margin-top: 16px;
  }
`;

const UserIcon = styled.div`
  border-radius: 100%;
  height: 8em;
  width: 8em;
  padding: 2em;
  box-sizing: border-box;
  background: var(--logoutBackground);
  margin: 32px auto;
`;

const Logout = styled.div`
  border-radius: 100%;
  height: 4em;
  width: 4em;
  padding: 1em;
  box-sizing: border-box;
  background: var(--logoutBackground);
  margin: 16px 0 16px auto;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
`;

const AccountDelete = styled.span`
  margin: 32px auto;
  text-decoration: underline;
  color: var(--linkColor);
`;
export default function User() {
  const { signOut, user, deleteUser } = useUser();
  const { markers } = useUserMarkers(user?.uid);
  const { closeOverlay } = useOverlay();

  return (
    <AuthenticatedPage title="Profile" padded withFooter>
      <UserIcon>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 22">
          <path
            fill="var(--icon)"
            d="M2 21l-.986.164A1 1 0 002 22v-1zm20 0v1a1 1 0 00.986-.836L22 21zm0-4l-.964.266L22 17zM2 17l.964.266L2 17zm0 5h20v-2H2v2zm20-1l.986.164v-.001l.001-.003.001-.007.004-.025.014-.091a17.906 17.906 0 00.148-1.408c.053-.83.065-1.971-.19-2.895l-1.928.532c.16.582.173 1.44.122 2.235a15.902 15.902 0 01-.145 1.335L22 21zm.964-4.266c-.68-2.466-2.355-3.926-4.39-4.74C16.583 11.196 14.22 11 12 11v2c2.146 0 4.202.198 5.83.85 1.583.633 2.725 1.673 3.206 3.416l1.928-.532zM12 11c-2.22 0-4.584.197-6.573.993-2.036.815-3.71 2.275-4.391 4.741l1.928.532c.48-1.743 1.623-2.783 3.206-3.416C7.798 13.198 9.854 13 12 13v-2zM1.036 16.734c-.255.924-.243 2.065-.19 2.894a17.905 17.905 0 00.167 1.536L2 21a482.092 482.092 0 01.986-.164v-.003l-.003-.017a7.731 7.731 0 01-.05-.354c-.03-.243-.066-.58-.09-.961-.052-.794-.04-1.653.121-2.235l-1.928-.532z"
          />
          <circle cx="12" cy="5" r="4" stroke="var(--icon)" strokeWidth="2" />
        </svg>
      </UserIcon>
      <Logout onClick={() => signOut().then(closeOverlay)}>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 47 48">
          <path
            stroke="var(--icon)"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2.5"
            d="M35 15l10 9.5L35 34"
          />
          <path
            stroke="var(--icon)"
            strokeLinecap="round"
            strokeWidth="2.5"
            d="M14.25 24.55h28.613"
          />
          <path
            stroke="var(--icon)"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="3"
            d="M36 2H2v44h34"
          />
        </svg>
      </Logout>
      <h2>Your Places</h2>
      <MarkerList>
        {markers?.length ? (
          markers?.map((m, i) => <MarkerCard deleteButton marker={m} key={i} />)
        ) : (
          <Empty>you added no places :(</Empty>
        )}
      </MarkerList>
      <AccountDelete
        onClick={() => {
          const confirm = window.confirm(
            "Sure you want to delete your Account?"
          );
          if (confirm) {
            deleteUser();
          }
        }}
      >
        delete your account
      </AccountDelete>
    </AuthenticatedPage>
  );
}
