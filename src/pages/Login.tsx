import Page from "../common/Page";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import TextField from "../common/TextField";
import styled from "styled-components";
import Button from "../common/Button";
import { useUser } from "../provider/UserProvider";
import Link from "../common/Link";

const Form = styled.form`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;
  ${Button} {
    margin: 32px;
  }
`;

type LoginData = {
  email?: string;
  password?: string;
};

type Props = {
  onSuccess?: () => void;
};

export default function Login({ onSuccess }: Props) {
  const [pending, setPending] = useState(false);
  const [signUp, setSignUp] = useState(false);
  const { register, handleSubmit } = useForm<LoginData>();
  const { login, signUp: signUpUser } = useUser();

  const onSubmit = handleSubmit(({ email, password }) => {
    setPending(true);
    if (signUp) {
      signUpUser(email!, password!)
        .then(() => {
          setPending(false);
          onSuccess && onSuccess();
        })
        .catch(() => {
          setPending(false);
        });
    } else {
      login(email!, password!)
        .then(() => {
          setPending(false);
          onSuccess && onSuccess();
        })
        .catch(() => {
          setPending(false);
        });
    }
  });

  return (
    <Page padded withFooter title={signUp ? "Registration" : "Login"}>
      <Form onSubmit={onSubmit}>
        <TextField
          type="email"
          placeholder="Email..."
          ref={register({ required: true })}
          name="email"
        />
        <TextField
          type="password"
          placeholder="Password..."
          ref={register({ required: true })}
          name="password"
        />
        <Button disabled={pending} type="submit">
          {signUp ? "Sign up" : "Login"}
        </Button>
      </Form>
      <Link onClick={() => setSignUp(!signUp)}>
        {signUp ? "Login" : "No Account? Sign up here"}
      </Link>
    </Page>
  );
}
