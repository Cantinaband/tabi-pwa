export interface Marker {
  id: string;
  geopoint: firebase.firestore.GeoPoint;
  description: string;
  category: Category;
  images: string[];
  creator: string;
  favorites?: string[];
  comments?: Comment[];
  rating?: Rating;
}

type Category = {
  name: string;
  icon: string;
};

type Rating = {
  value: number;
  numberOfRatings: number;
};

export type Comment = {
  message: string;
  rating: number;
  createdAt: firebase.firestore.Timestamp;
  userId: string;
};

export interface AddMarker extends Marker {
  id?: string;
  images: File[];
  creator: string;
}
