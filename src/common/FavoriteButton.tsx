import styled from "styled-components";
import React from "react";
import { useUser } from "../provider/UserProvider";
import { addToFavorites, removeFromFavorites } from "../firebase";
import { Marker } from "../typings/marker";

const Container = styled.div`
  height: 32px;
  width: 32px;
`;

type Props = {
  className?: string;
  marker?: Marker;
};

export default function FavoriteButton({ className, marker }: Props) {
  const { user } = useUser();
  const isFavorite = () => {
    if (marker?.favorites?.includes(user!.uid)) {
      return true;
    } else {
      return false;
    }
  };
  return user ? (
    <Container
      className={className}
      onClick={(e) => {
        e.stopPropagation();
        if (isFavorite()) {
          const confirm = window.confirm(
            "Sure you want to remove this place from your favorites?"
          );
          if (confirm) {
            removeFromFavorites(marker!, user!.uid);
          }
        } else {
          addToFavorites(marker!, user!.uid);
        }
      }}
    >
      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 22 18">
        <defs />
        <path
          fill={isFavorite() ? "var(--warn)" : "transparent"}
          fillRule="evenodd"
          d="M19.115 8.883a3.717 3.717 0 000-4.997l-1.33-1.465a3.68 3.68 0 00-5.452 0L11 3.89 9.667 2.42a3.68 3.68 0 00-5.451 0l-1.33 1.466a3.717 3.717 0 000 4.997l1.791 1.974-.02.023 4.687 5.165A2.2 2.2 0 0011 16.77a2.2 2.2 0 001.656-.725l4.687-5.165-.02-.023 1.792-1.974z"
          clipRule="evenodd"
        />
        <path
          fill={isFavorite() ? "var(--warn)" : "var(--icon)"}
          d="M19.115 3.886l.74-.672-.74.672zm0 4.997l.74.672-.74-.672zm-1.33-6.462l-.741.672.74-.672zm-5.452 0l.74.672-.74-.672zM11 3.89l-.74.672.74.816.74-.816-.74-.673zM9.667 2.42l.74-.671-.74.672zm-5.451 0l.74.673-.74-.672zm-1.33 1.466l-.741-.672.74.672zm0 4.997l.74-.672-.74.672zm1.791 1.974l.74.672.61-.672-.61-.672-.74.672zm-.02.023l-.74-.672-.61.672.61.672.74-.672zm4.687 5.165l-.74.672.74-.672zM11 16.77l.01-1h-.02l.01 1zm1.656-.725l-.74-.672.74.672zm4.687-5.165l.74.672.61-.672-.61-.672-.74.672zm-.02-.023l-.74-.672-.61.672.61.672.74-.672zm1.051-6.299a2.717 2.717 0 010 3.653l1.481 1.344a4.717 4.717 0 000-6.34l-1.48 1.343zm-1.33-1.465l1.33 1.465 1.481-1.344-1.33-1.465-1.481 1.344zm-3.97 0a2.68 2.68 0 013.97 0l1.481-1.344a4.68 4.68 0 00-6.932 0l1.48 1.344zM11.74 4.562l1.333-1.47-1.482-1.343-1.333 1.468 1.482 1.345zm0-1.345L10.408 1.75 8.926 3.093l1.333 1.469 1.482-1.345zM10.408 1.75a4.68 4.68 0 00-6.933 0l1.481 1.344a2.68 2.68 0 013.97 0l1.482-1.344zm-6.933 0l-1.33 1.465 1.481 1.344 1.33-1.465-1.481-1.344zm-1.33 1.465a4.717 4.717 0 000 6.34l1.481-1.343a2.717 2.717 0 010-3.653L2.145 3.214zm0 6.34l1.792 1.975 1.48-1.344-1.791-1.974-1.481 1.344zm3.252 1.998l.02-.023-1.48-1.344-.02.023 1.48 1.344zm4.688 3.821l-4.688-5.165-1.48 1.344 4.687 5.165 1.48-1.344zm.906.397a1.2 1.2 0 01-.906-.397l-1.481 1.344a3.2 3.2 0 002.405 1.053l-.018-2zm0 2a3.2 3.2 0 002.405-1.053l-1.48-1.344a1.2 1.2 0 01-.907.397l-.018 2zm2.405-1.053l4.688-5.165-1.481-1.344-4.688 5.165 1.482 1.344zm4.688-6.51l-.02-.022-1.482 1.344.02.023 1.482-1.344zm.29-1.996l-1.792 1.974 1.481 1.344 1.792-1.974-1.48-1.344z"
        />
      </svg>
    </Container>
  ) : null;
}
