import styled from "styled-components";
import React, { ReactNode } from "react";

const Input = styled.input<{ ref: any }>`
  display: none;
`;
const Field = styled.label`
  height: 20px;
`;

type Props = {
  className?: string;
  children: ReactNode;
  name: string;
  onChange?: (files: File[]) => void;
};

const FileField = React.forwardRef(
  ({ className, children, name, onChange }: Props, ref) => {
    return (
      <>
        <Input
          type="file"
          multiple
          id="file-input"
          ref={ref}
          name={name}
          onChange={(e) =>
            onChange && onChange(Array.from(e.target.files || []))
          }
        />
        <Field htmlFor="file-input" className={className}>
          {children}
        </Field>
      </>
    );
  }
);
export default FileField;
