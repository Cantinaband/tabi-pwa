import React from "react";

export function MenuDots({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill="var(--icon)"
        d="M12 7a2 2 0 100-4 2 2 0 000 4zM12 14a2 2 0 100-4 2 2 0 000 4zM12 21a2 2 0 100-4 2 2 0 000 4z"
      />
    </svg>
  );
}

export function Menu({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="var(--text)"
        strokeWidth="2.5"
        d="M5 12.5h14M7 18h10M2 7h20"
      />
    </svg>
  );
}

export function User({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 16 16"
    >
      <path stroke="var(--icon)" strokeWidth="2" d="M14 15a6 6 0 00-12 0" />
      <path
        stroke="var(--icon)"
        strokeWidth="2"
        d="M8 8a3 3 0 100-6 3 3 0 000 6z"
      />
    </svg>
  );
}

export function Heart({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 18 16"
    >
      <path
        fill="var(--icon)"
        d="M2.962 3.41l-.74-.673.74.672zm0 4.996l-.74.672.74-.672zm.065-5.069l.74.672-.74-.672zm5.452 0l.74-.672-.74.672zM9 3.912l-.74.672L9 5.4l.74-.816L9 3.912zm.521-.575l-.74-.672.74.672zm5.452 0l-.74.672.74-.672zm.065.072l.74-.672-.74.672zm0 4.997l-.74-.672.74.672zm-.98 1.08l-.74-.672-.61.672.61.672.74-.672zm.017.018l.74.672.61-.672-.61-.672-.74.672zm-3.423 3.772l-.74-.672.74.672zM9 14l.007-1h-.014L9 14zm-1.652-.724l-.74.672.74-.672zM3.925 9.504l-.74-.672-.61.672.61.672.74-.672zm.017-.018l.74.672.61-.672-.61-.672-.74.672zm-1.72-6.749a4.717 4.717 0 000 6.34l1.48-1.343a2.717 2.717 0 010-3.653l-1.48-1.344zm.065-.072l-.066.072 1.481 1.344.066-.072-1.481-1.344zm6.932 0a4.68 4.68 0 00-6.932 0l1.48 1.344a2.68 2.68 0 013.971 0L9.22 2.665zm.522.575l-.522-.575L7.74 4.01l.52.575L9.741 3.24zm0 1.344l.52-.575-1.48-1.344-.522.575 1.482 1.344zm.52-.575a2.68 2.68 0 013.971 0l1.481-1.344a4.68 4.68 0 00-6.932 0l1.48 1.344zm3.971 0l.066.072 1.48-1.344-.065-.072-1.48 1.344zm.066.072a2.718 2.718 0 010 3.653l1.48 1.344a4.718 4.718 0 000-6.34l-1.48 1.343zm0 3.653l-.98 1.08 1.48 1.344.98-1.08-1.48-1.344zm.517 1.098l-.016-.018-1.481 1.344.016.018 1.481-1.344zm-3.423 5.116l3.423-3.772-1.48-1.344-3.424 3.772 1.481 1.344zM8.992 15a3.2 3.2 0 002.4-1.052l-1.48-1.344a1.2 1.2 0 01-.905.396l-.014 2zm0-2a1.2 1.2 0 01-.903-.396l-1.481 1.344A3.2 3.2 0 009.008 15l-.015-2zm-.903-.396L4.666 8.832l-1.481 1.344 3.423 3.772 1.48-1.344zm-3.423-2.428l.016-.018-1.48-1.344-.017.018 1.48 1.344zM2.22 9.078l.98 1.08 1.481-1.344-.98-1.08-1.48 1.344z"
      />
    </svg>
  );
}

export function Add({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 12 12"
    >
      <path stroke="var(--icon)" strokeWidth="2" d="M0 6h12M6 0v12" />
    </svg>
  );
}

export function Compass({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="var(--icon)"
        strokeLinejoin="round"
        strokeWidth="2.5"
        d="M16.707 16.707L22 3 8.293 8.293 3 22l13.707-5.293z"
      />
      <path stroke="var(--icon)" strokeWidth="2.5" d="M8 8l9 9" />
    </svg>
  );
}

export function Search({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke="var(--icon)"
        strokeWidth="2.5"
        d="M13.286 19.429a7.714 7.714 0 100-15.429 7.714 7.714 0 000 15.429z"
      />
      <path stroke="var(--icon)" strokeWidth="2.5" d="M7.286 16.857L3 20.286" />
    </svg>
  );
}

export function Start({ ...props }) {
  return (
    <svg
      {...props}
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill="currentColor"
        stroke="var(--icon)"
        strokeWidth="2"
        d="M12.055 17.104l-.529-.329-.528.329-4.41 2.743 1.248-5.004.15-.603-.473-.402-3.992-3.385 5.118-.356.616-.043.238-.57 2.033-4.883 2.034 4.883.238.57.616.043 5.117.356-3.991 3.386-.474.401.15.603 1.248 5.004-4.41-2.743z"
      />
    </svg>
  );
}
