import React from "react";
import { useUser } from "../provider/UserProvider";
import Login from "../pages/Login";
import Page, { PageProps } from "./Page";

export default function AuthenticatedPage({ children, ...rest }: PageProps) {
  const { user } = useUser();

  return <>{user ? <Page {...rest}>{children}</Page> : <Login />}</>;
}
