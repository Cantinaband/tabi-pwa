import styled from "styled-components";
import React from "react";
import ThemeSwitch from "./ThemeSwitch";

const Container = styled.footer`
  display: flex;
  flex: 0 0 auto;
  padding: 16px;
  background: var(--footerBackground);
  margin: auto -16px -16px -16px;
`;

export default function Footer() {
  return (
    <Container>
      <ThemeSwitch />
    </Container>
  );
}
