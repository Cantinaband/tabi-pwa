import React from "react";
import styled, { keyframes } from "styled-components";

const dash = keyframes`
  0% {
    stroke-dashoffset: 745.74853515625;
  }
  100% {
    stroke-dashoffset: 0;
  
    }`;

const Check = styled.svg`
  width: 100%;
  path {
    stroke: var(--buttonBackground);
    stroke-dashoffset: 745.74853515625;
    stroke-dasharray: 745.74853515625;
    animation: ${dash} 2s ease-out forwards;
  }
`;
export default function AnimatedCheck() {
  return (
    <Check
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      viewBox="0 0 98.5 98.5"
      enableBackground="new 0 0 98.5 98.5"
      xmlSpace="preserve"
    >
      <path
        fill="none"
        strokeWidth="8"
        strokeMiterlimit="10"
        d="M81.7,17.8C73.5,9.3,62,4,49.2,4C24.3,4,4,24.3,4,49.2s20.3,45.2,45.2,45.2s45.2-20.3,45.2-45.2c0-8.6-2.4-16.6-6.5-23.4l0,0L45.6,68.2L24.7,47.3"
      />
    </Check>
  );
}
