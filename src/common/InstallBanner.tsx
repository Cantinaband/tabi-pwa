import React, { useState } from "react";
import styled from "styled-components";
import { isIosDevice, isInstalled } from "../utils";
import RoundClose from "./RoundClose";

const Container = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: space-between;
  bottom: 1em;
  left: 20%;
  right: 20%;
  padding: 1em;
  background: var(--cardBackground);
  border-radius: 8px;
  ::after {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    border-width: 10px;
    border-style: solid;
    border-color: var(--cardBackground) transparent transparent transparent;
    top: 100%;
    left: 50%;
    transform: translateX(-50%);
  }
`;

const Close = styled(RoundClose)`
  flex: 1 0 auto;
  background: var(--warn);
  max-width: 1.5em;
  max-height: 1.5em;
  margin-left: 10px;
`;

export default function InstallBanner() {
  const [showIosInstallBanner, setShowIosInstallBanner] = useState(
    isIosDevice() && !isInstalled()
  );

  if (showIosInstallBanner) {
    return (
      <Container>
        Install this PWA on your iPhone: tap here and then "Add to homescreen".
        <Close onClick={() => setShowIosInstallBanner(false)} />
      </Container>
    );
  }

  return null;
}
