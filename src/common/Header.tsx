import React, { useState } from "react";
import styled from "styled-components";
import { Menu } from "./Icon";
import UserPage from "../pages/User";
import { getAllFilters } from "../utils";
import GeoAutocomplete from "./GeoAutocomplete";
import { useMap } from "../Map/MapProvider";
import { useOverlay } from "../provider/OverlayPageProvider";
import Filters from "./Filters";

const Container = styled.div`
  display: flex;
  flex: 1 0 auto;
  position: relative;
  z-index: 2;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items: center;
  background: var(--headerBackground);
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  padding: 16px;
`;

const User = styled.div`
  border-radius: 100%;
  height: 40px;
  width: 40px;
  padding: 8px;
  box-sizing: border-box;
  background: var(--interactionBackground);
`;

const MenuButton = styled.div<{ active: boolean }>`
  height: 40px;
  width: 40px;
  box-sizing: border-box;
  padding: 8px;
  border-radius: 100%;
  background: ${(p) =>
    p.active ? "var(--interactionBackground)" : "var(--headerBackground)"};
  transition: background 200ms ease-in-out;
`;

const StyledFilters = styled(Filters)<{ visible: boolean }>`
  position: absolute;
  z-index: 1;
  transform: ${(p) => (p.visible ? "translateY(100%)" : "translateY(0%)")};
  transition: transform 400ms ease-in-out;
`;

export default function Header() {
  const [filterMenu, setFilterMenu] = useState(false);
  const { setOverlay } = useOverlay();
  const { setSelectedFilters, addPlaceLocationMode } = useMap();
  return (
    <>
      <Container>
        {!addPlaceLocationMode && (
          <User onClick={() => setOverlay(<UserPage />)}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 22"
            >
              <path
                fill="var(--icon)"
                d="M2 21l-.986.164A1 1 0 002 22v-1zm20 0v1a1 1 0 00.986-.836L22 21zm0-4l-.964.266L22 17zM2 17l.964.266L2 17zm0 5h20v-2H2v2zm20-1l.986.164v-.001l.001-.003.001-.007.004-.025.014-.091a17.906 17.906 0 00.148-1.408c.053-.83.065-1.971-.19-2.895l-1.928.532c.16.582.173 1.44.122 2.235a15.902 15.902 0 01-.145 1.335L22 21zm.964-4.266c-.68-2.466-2.355-3.926-4.39-4.74C16.583 11.196 14.22 11 12 11v2c2.146 0 4.202.198 5.83.85 1.583.633 2.725 1.673 3.206 3.416l1.928-.532zM12 11c-2.22 0-4.584.197-6.573.993-2.036.815-3.71 2.275-4.391 4.741l1.928.532c.48-1.743 1.623-2.783 3.206-3.416C7.798 13.198 9.854 13 12 13v-2zM1.036 16.734c-.255.924-.243 2.065-.19 2.894a17.905 17.905 0 00.167 1.536L2 21a482.092 482.092 0 01.986-.164v-.003l-.003-.017a7.731 7.731 0 01-.05-.354c-.03-.243-.066-.58-.09-.961-.052-.794-.04-1.653.121-2.235l-1.928-.532z"
              />
              <circle
                cx="12"
                cy="5"
                r="4"
                stroke="var(--icon)"
                strokeWidth="2"
              />
            </svg>
          </User>
        )}
        <GeoAutocomplete />
        {!addPlaceLocationMode && (
          <MenuButton
            active={filterMenu}
            onClick={() => setFilterMenu(!filterMenu)}
          >
            <Menu />
          </MenuButton>
        )}
      </Container>
      <StyledFilters
        visible={filterMenu}
        padding="16px"
        filter={getAllFilters()}
        onFilterChange={setSelectedFilters}
        multipleSelect
      />
    </>
  );
}
