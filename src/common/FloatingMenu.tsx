import React, { useState, useRef, useEffect } from "react";
import styled, { css } from "styled-components";
import { MenuDots, User as UserIcon, Heart, Add } from "../common/Icon";
import useOnClickOutside from "../hooks/useOnClickOutside";
import { useMap } from "../Map/MapProvider";
import { useUser } from "../provider/UserProvider";
import { useOverlay } from "../provider/OverlayPageProvider";
import Login from "../pages/Login";
import RoundClose from "./RoundClose";
import User from "../pages/User";
import Favorites from "../pages/Favorites";

const expandedStyle = css`
  div:nth-child(2) {
    transition-delay: 200ms;
    transform: translate3d(-50%, -150%, 0) scale(1);
  }
  div:nth-child(3) {
    transition-delay: 100ms;
    transform: translate3d(-150%, 0%, 0) scale(1);
  }
  div:nth-child(4) {
    transform: translate3d(-50%, 150%, 0) scale(1);
  }
`;
const Container = styled.div<{ expanded: boolean }>`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  ${(p) => p.expanded && expandedStyle}
`;

const MainIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 3rem;
  height: 3rem;
  box-sizing: border-box;
  z-index: 1;
  padding: 5px;
  border-radius: 100%;
  background: var(--mapButtonBackground);
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  svg {
    width: 100%;
  }
`;

const FloatingIcon = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 2px 12px rgba(0, 0, 0, 0.12);
  box-sizing: border-box;
  padding: 6px;
  position: absolute;
  transition: all 200ms ease-in-out;
  width: 2rem;
  height: 2rem;
  background: var(--mapButtonBackground);
  border-radius: 100%;
  svg {
    width: 100%;
  }
`;
const Close = styled(RoundClose)`
  box-shadow: none;
  width: 2.5rem;
  height: 2.5rem;
  background: var(--mapButtonBackground);
`;
type Props = {
  className?: string;
};
export default function FloatingMenu({ className }: Props) {
  const [expanded, setExpanded] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);
  const { setAddPlaceLocationMode, addPlaceLocationMode } = useMap();
  const { user } = useUser();
  const { setOverlay, closeOverlay } = useOverlay();
  useOnClickOutside(containerRef, () => setExpanded(false));
  useEffect(() => {
    if (addPlaceLocationMode) {
      setExpanded(false);
    }
  }, [addPlaceLocationMode]);
  return (
    <Container className={className} expanded={expanded} ref={containerRef}>
      <MainIcon
        onClick={() =>
          addPlaceLocationMode
            ? setAddPlaceLocationMode(false)
            : setExpanded(!expanded)
        }
      >
        {addPlaceLocationMode ? <Close /> : <MenuDots />}
      </MainIcon>
      <FloatingIcon
        onClick={() => {
          setOverlay(<Favorites />);
        }}
      >
        <Heart />
      </FloatingIcon>
      <FloatingIcon
        onClick={() => {
          setOverlay(<User />);
        }}
      >
        <UserIcon />
      </FloatingIcon>
      <FloatingIcon
        onClick={() =>
          user
            ? setAddPlaceLocationMode(true)
            : setOverlay(
                <Login
                  onSuccess={() => {
                    closeOverlay();
                    setAddPlaceLocationMode(true);
                  }}
                />
              )
        }
      >
        <Add />
      </FloatingIcon>
    </Container>
  );
}
