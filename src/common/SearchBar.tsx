import React from "react";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  flex: 1 1 0%;
  justify-content: flex-end;
  height: 100%;
`;

const Input = styled.input`
  position: relative;
  border: none;
  outline: none;
  padding: 0 0 0 8px;
  margin: 0 8px;
  width: 32px;
  font-size: 16px;
  color: var(--text);
  border-radius: 12px;
  background-color: var(--headerBackground);
  transition: width 400ms 0s, background-color 400ms 400ms;
  ::-webkit-input-placeholder {
    opacity: 0;
    transition: opacity 200ms ease-in;
  }
  ::placeholder {
    color: rgba(var(--textRgb), 0.5);
  }
  :focus,
   :not(:placeholder-shown) {
    transition: width 400ms 0s, background-color 400ms 0ms;
    background-color: var(--interactionBackground);
    padding: 0 40px 0 16px;
    width: 100%;
    ::-webkit-input-placeholder {
      opacity: 1;
      transition-delay: 300ms;
    }
  }
`;

const Icon = styled.svg`
  position: absolute;
  width: 22px;
  height: 22px;
  top: 50%;
  right: 16px;
  pointer-events: none;
  transform: translateY(-50%);
`;
type Props = {
  onChange: (value: string) => void;
};

export default function SearchBar({ onChange }: Props) {
  return (
    <Container>
      <Input
        type="text"
        placeholder="search some place..."
        onChange={(e) => onChange(e.target.value)}
      />
      <Icon xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 21 20">
        <path
          stroke="var(--icon)"
          strokeWidth="2.5"
          d="M11.286 17.429a7.714 7.714 0 100-15.429 7.714 7.714 0 000 15.429z"
          clipRule="evenodd"
        />
        <path
          stroke="var(--icon)"
          strokeWidth="2.5"
          d="M5.286 14.857L1 18.286"
        />
      </Icon>
    </Container>
  );
}
