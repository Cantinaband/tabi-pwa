import styled from "styled-components";

const Link = styled.a`
  font-size: 16px;
  text-decoration: underline;
  color: var(--linkColor);
`;

export default Link;
