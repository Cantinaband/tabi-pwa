import styled from "styled-components";

const Empty = styled.h3`
  padding: 32px;
  text-align: center;
  color: rgba(var(--textRgb), 0.3);
`;

export default Empty;
