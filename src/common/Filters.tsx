import styled from "styled-components";
import HorizontalSlider, { HorizontalSliderProps } from "./HorizontalSlider";
import { Category } from "../typings/marker";
import React, { useState, useEffect } from "react";

const Container = styled(HorizontalSlider)`
  background: var(--filterBackground);
`;

const Filter = styled.div<{ selected: boolean }>`
  flex: 0 0 auto;
  padding: 8px 16px;
  border-radius: 100px;
  margin-right: 16px;
  border: 1px solid
    ${(p) => (p.selected ? "var(--filterBackground)" : "var(--filterBorder)")};
  ${(p) => p.selected && "color: var(--buttonText);"};
  background: ${(p) =>
    p.selected ? "var(--buttonBackground)" : "var(--filterBackground)"};
`;

type Props = {
  filter: Category[];
  className?: string;
  onFilterChange: (filter: Category[]) => void;
  multipleSelect?: boolean;
} & HorizontalSliderProps;

export default function Filters({
  filter,
  className,
  onFilterChange,
  multipleSelect,
  ...rest
}: Props) {
  const [selected, setSelected] = useState<Category[]>([]);

  const handleFilterClick = (e: Category) => {
    if (multipleSelect) {
      let data = [...selected];
      const index = data.findIndex((f) => f.name === e.name);
      if (index === -1) {
        setSelected([...data, e]);
      } else {
        data.splice(index, 1);
        setSelected(data);
      }
    } else {
      setSelected([e]);
    }
  };
  useEffect(() => {
    onFilterChange(selected);
  }, [onFilterChange, selected]);
  return (
    <Container {...rest} className={className}>
      {filter.map((f, i) => (
        <Filter
          key={i}
          selected={selected.some((c) => c.name === f.name)}
          onClick={() => handleFilterClick(f)}
        >
          {f.name}
        </Filter>
      ))}
    </Container>
  );
}
