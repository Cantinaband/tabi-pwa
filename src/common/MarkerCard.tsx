import { Marker } from "../typings/marker";
import HorizontalSlider from "./HorizontalSlider";
import styled from "styled-components";
import React from "react";
import Rating from "./Rating";
import RoundClose from "./RoundClose";
import { deleteMarker } from "../firebase";
import { useMap } from "../Map/MapProvider";
import useImages from "../hooks/useImages";
import { useOverlay } from "../provider/OverlayPageProvider";
import FavoriteButton from "./FavoriteButton";

const Container = styled.div`
  border-radius: 8px;
  box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 0.2);
`;
const Content = styled.div`
  padding: 16px;
  background: var(--cardBackground);
  border-top: none;
  border-radius: 0 0 8px 8px;
`;
const Gallery = styled(HorizontalSlider)`
  height: 100px;
  border-radius: 8px 8px 0 0;
  background: var(--imageSliderBackground);
`;

const Description = styled.p`
  position: relative;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: clip ellipsis;
`;
const Image = styled.img`
  object-fit: cover;
  width: 70%;
`;

const Delete = styled(RoundClose)`
  background: var(--warn);
  box-shadow: unset;
  margin-left: auto;
  svg > path {
    stroke: var(--buttonText);
  }
`;

const Favorite = styled(FavoriteButton)`
  margin-left: auto;
`;
type Props = {
  marker: Marker;
  deleteButton?: boolean;
  favoriteButton?: boolean;
};
export default function MarkerCard({
  marker,
  deleteButton,
  favoriteButton
}: Props) {
  const { setSelectedMarker } = useMap();
  const { closeOverlay } = useOverlay();
  const { images } = useImages(marker.images);
  return (
    <Container
      onClick={() => {
        closeOverlay();
        setSelectedMarker(marker);
      }}
    >
      <Gallery padding="0">
        {images.map((image, i) => (
          <Image src={image} key={i} />
        ))}
      </Gallery>
      <Content>
        <Rating max={5} rating={marker.rating?.value} />
        <Description>{marker.description}</Description>
        {deleteButton && (
          <Delete
            onClick={(e) => {
              e.stopPropagation();
              const confirm = window.confirm(
                "Sure you want to delete this place?"
              );
              if (confirm) {
                deleteMarker(marker);
              }
            }}
          />
        )}
        {favoriteButton && <Favorite marker={marker} />}
      </Content>
    </Container>
  );
}
