import styled from "styled-components";
import React from "react";

const Container = styled.div`
  position: relative;
  width: 100%;
  margin-top: 20px;
`;
const Input = styled.input<{ ref: any }>`
  font-size: 20px;
  width: 100%;
  outline: none;
  border: none;
  border-bottom: 2px solid var(--inputBorder);
  margin-bottom: 16px;
  padding: 4px 0;
  border-radius: 0;
  background: var(--inputBackground);
  color: var(--text);
  :not(:placeholder-shown) + label,
  :focus + label {
    transform: translateY(calc(-100% - 4px)) scale(0.7);
  }
  :focus,
  :not(:placeholder-shown) {
    border-bottom: 2px solid var(--inputBorderFocus);
  }
  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus,
  :-webkit-autofill:active {
    -webkit-text-fill-color: var(--text);
    box-shadow: 0 0 0 30px var(--inputBackground) inset !important;
  }
`;

const Label = styled.label`
  position: absolute;
  font-size: 20px;
  top: 4px;
  left: 0;
  color: var(--inputBorder);
  transition: all 200ms ease-in-out;
  pointer-events: none;
  transform-origin: 0 center;
`;

type Props = {
  type: "email" | "password" | "text";
  placeholder: string;
  name: string;
};

const TextField = React.forwardRef(
  ({ placeholder, name, type }: Props, ref) => {
    return (
      <Container>
        <Input type={type} name={name} ref={ref} placeholder=" " />
        <Label>{placeholder}</Label>
      </Container>
    );
  }
);
export default TextField;
