import { ReactNode, useEffect } from "react";
import React from "react";
import styled from "styled-components";
import { useOverlay } from "../provider/OverlayPageProvider";
import Footer from "./Footer";

const Container = styled.div<{ padded?: boolean }>`
  display: flex;
  flex-direction: column;
  height: 100%;
  background: var(--pageBackground);
  padding: ${(p) => (p.padded ? "16px" : 0)};
  overflow-y: scroll;
`;

export type PageProps = {
  children: ReactNode;
  padded?: boolean;
  title?: string;
  withFooter?: boolean;
};

export default function Page({
  children,
  padded,
  title,
  withFooter
}: PageProps) {
  const { setTitle } = useOverlay();
  useEffect(() => {
    setTitle(title);
  }, [setTitle, title]);
  return (
    <Container padded={padded}>
      {children}
      {withFooter && <Footer />}
    </Container>
  );
}
